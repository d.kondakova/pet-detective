package com.example.pd_android.api.model

import retrofit2.http.*

interface SWApiService {

    @GET("missing")
    suspend fun getMissing(
    ): List<ApiDataModel>

    @GET("finding")
    suspend fun getFinding(
    ): List<ApiDataModel>

    @POST("add_finding")
    suspend fun postFinding(
        @Body item: ApiDataModel
    ): Int

    @POST("add_missing")
    suspend fun postMissing(
        @Body item: ApiDataModel
    ): Int

    @GET("finding_filter")
    suspend fun getFindingFilter(
        @Query("kindOfAnimal") kindOfAnimal: String,
        @Query("colorOfAnimal") colorOfAnimal: String,
        @Query("city") city: String,
        @Query("district") district: String,
    ): List<ApiDataModel>

    @GET("missing_filter")
    suspend fun getMissingFilter(
        @Query("kindOfAnimal") kindOfAnimal: String,
        @Query("colorOfAnimal") colorOfAnimal: String,
        @Query("city") city: String,
        @Query("district") district: String,
    ): List<ApiDataModel>

    @DELETE("{id}")
    suspend fun delPost(
        @Path("id") id: Int
    )

}