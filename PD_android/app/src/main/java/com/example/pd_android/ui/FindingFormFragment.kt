package com.example.pd_android.ui

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.pd_android.R
import com.example.pd_android.api.model.ApiDataModel
import com.example.pd_android.databinding.FragmentFindingBinding
import com.example.pd_android.databinding.FragmentFindingFormBinding
import com.example.pd_android.viewmodel.FindingFormViewModel
import com.example.pd_android.viewmodel.FindingViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FindingFormFragment : Fragment() {

    private lateinit var binding: FragmentFindingFormBinding
    private val viewModel by viewModels<FindingFormViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentFindingFormBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



        binding.button.setOnClickListener {
            try {
                val item: ApiDataModel = ApiDataModel(
                    0,
                    binding.kindOfAnimal.text.toString(),
                    binding.colorOfAnimal.text.toString(),
                    binding.city.text.toString(),
                    binding.district.text.toString(),
                    binding.author.text.toString(),
                    null,
                    1,
                    null
                )
                Log.d("dbbd", "button")
                viewModel.postFinding(item)

                val navController = findNavController()
                navController.navigate(R.id.findingFragment)

            } catch (e: Exception){Log.d("dbbd", e.toString())}
        }

    }

}