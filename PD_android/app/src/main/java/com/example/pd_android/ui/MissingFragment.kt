package com.example.pd_android.ui

import android.app.AlertDialog
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pd_android.R
import com.example.pd_android.api.model.ApiDataModel
import com.example.pd_android.databinding.FragmentMissingBinding
import com.example.pd_android.ui.adapter.FindingAdapter
import com.example.pd_android.ui.adapter.SpacingItemDecoration
import com.example.pd_android.viewmodel.MissingViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MissingFragment : Fragment() {

    private lateinit var binding: FragmentMissingBinding
    private val viewModel by viewModels<MissingViewModel>()
    private lateinit var adapter: FindingAdapter
    var filter: List<String> = emptyList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMissingBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.list.addItemDecoration(SpacingItemDecoration(dpToPx(8)))


        adapter = FindingAdapter(::del, ::go)
        viewModel.posts.observe(viewLifecycleOwner) {
            adapter.submitList(it)
        }

        binding.list.adapter = adapter
        binding.list.layoutManager = LinearLayoutManager(requireContext())
        viewModel.getPosts()

        binding.button4.setOnClickListener { withEditText() }

    }

    fun withEditText() {
        val builder = AlertDialog.Builder(requireContext())
        val inflater = layoutInflater
        builder.setTitle("Missing Filter")
        val dialogLayout = inflater.inflate(R.layout.dialog, null)
        val editText1 = dialogLayout.findViewById<EditText>(R.id.editText1)
        val editText2 = dialogLayout.findViewById<EditText>(R.id.editText2)
        val editText3 = dialogLayout.findViewById<EditText>(R.id.editText3)
        val editText4 = dialogLayout.findViewById<EditText>(R.id.editText4)
        builder.setView(dialogLayout)
        builder.setPositiveButton("OK") { dialogInterface, i ->
            filter = listOf(
                editText1.text.toString(),
                editText2.text.toString(),
                editText3.text.toString(),
                editText4.text.toString()
            )
            Log.d("dbbd", filter.toString())
            viewModel.getPostsWithFilter(filter[0],filter[1],filter[2],filter[3])
        }
        builder.show()
    }

    fun dpToPx(dp: Int): Int {
        val displayMetrics: DisplayMetrics = this.getResources().getDisplayMetrics()
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))
    }

    fun del(item: Int) {
        viewModel.del(item)
        viewModel.getPosts()
    }

    fun go(item: ApiDataModel) {
        val bundle = Bundle()
        bundle.putParcelable("data", item)
        val navController = findNavController()
        navController.navigate(R.id.singleFragment, bundle)
    }

}