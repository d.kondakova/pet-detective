package com.example.pd_android.ui

import android.app.AlertDialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.DisplayMetrics
import android.util.Log
import android.view.*
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pd_android.R
import com.example.pd_android.api.model.ApiDataModel
import com.example.pd_android.databinding.FragmentFindingBinding
import com.example.pd_android.databinding.FragmentMissingBinding
import com.example.pd_android.ui.adapter.FindingAdapter
import com.example.pd_android.ui.adapter.SpacingItemDecoration
import com.example.pd_android.viewmodel.FindingViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FindingFragment : Fragment() {

    private lateinit var binding: FragmentFindingBinding
    private val viewModel by viewModels<FindingViewModel>()
    private lateinit var adapter: FindingAdapter
    var filter: List<String> = emptyList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentFindingBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.list.addItemDecoration(SpacingItemDecoration(dpToPx(8)))


        adapter = FindingAdapter(::del, ::go)
        viewModel.posts.observe(viewLifecycleOwner) {
            adapter.submitList(it)
        }

        binding.list.adapter = adapter
        binding.list.layoutManager = LinearLayoutManager(requireContext())
        viewModel.getPosts()

        binding.button3.setOnClickListener { withEditText() }

    }

    fun withEditText() {
        val builder = AlertDialog.Builder(requireContext())
        val inflater = layoutInflater
        builder.setTitle("Finding Filter")
        val dialogLayout = inflater.inflate(R.layout.dialog, null)
        val editText1 = dialogLayout.findViewById<EditText>(R.id.editText1)
        val editText2 = dialogLayout.findViewById<EditText>(R.id.editText2)
        val editText3 = dialogLayout.findViewById<EditText>(R.id.editText3)
        val editText4 = dialogLayout.findViewById<EditText>(R.id.editText4)
        builder.setView(dialogLayout)
        builder.setPositiveButton("OK") { dialogInterface, i ->
            filter = listOf(
                editText1.text.toString(),
                editText2.text.toString(),
                editText3.text.toString(),
                editText4.text.toString()
            )
            Log.d("dbbd", filter.toString())
            viewModel.getPostsWithFilter(filter[0],filter[1],filter[2],filter[3])
        }
        builder.show()
    }

    fun dpToPx(dp: Int): Int {
        val displayMetrics: DisplayMetrics = this.getResources().getDisplayMetrics()
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))
    }

    fun del(item: Int) {
        basicAlert(item)
    }

    fun basicAlert(item: Int){

        val builder = AlertDialog.Builder(requireContext())

        with(builder)
        {
            setTitle("Alert!!!")
            setMessage("Are you sure?")
            setPositiveButton("YES") { dialogInterface, i ->
                viewModel.del(item)
                viewModel.getPosts()
            }
            setNegativeButton("NO") { dialogInterface, i ->
                Log.d("dbbd", "close")
            }
            show()
        }
    }

    fun go(item: ApiDataModel) {
        val bundle = Bundle()
        bundle.putParcelable("data", item)
        val navController = findNavController()
        navController.navigate(R.id.singleFragment, bundle)
    }

}