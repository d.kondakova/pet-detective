package com.example.pd_android.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pd_android.api.model.ApiDataModel
import com.example.pd_android.api.model.SWApiService
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

@HiltViewModel
class FindingFormViewModel @Inject constructor(
    private val apiService: SWApiService
) : ViewModel() {

    fun postFinding(item: ApiDataModel) {
        viewModelScope.launch {
            val value = try {apiService.postFinding(item) }
            catch ( e: Exception) { 0 }
        }
    }

}