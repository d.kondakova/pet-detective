package com.example.pd_android.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pd_android.api.model.ApiDataModel
import com.example.pd_android.api.model.SWApiService
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

@HiltViewModel
class MissingViewModel @Inject constructor(
    private val apiService: SWApiService
) : ViewModel() {

    val posts = MutableLiveData<List<ApiDataModel>>()

    fun getPosts() {
        viewModelScope.launch {
            posts.value = try {apiService.getMissing() }
            catch ( e: Exception) { emptyList<ApiDataModel>()}
            Log.d("dbbd", posts.value.toString())
        }
    }

    fun getPostsWithFilter(item1: String,item2: String,item3: String,item4: String) {
        viewModelScope.launch {
            posts.value = try {
                apiService.getMissingFilter(item1,item2,item3,item4)
            } catch (e: Exception) {
                Log.d("dbbd", e.toString())
                emptyList<ApiDataModel>()
            }
            Log.d("dbbd", posts.value.toString())
        }
    }

    fun del(id: Int) {
        viewModelScope.launch {
            try {
                apiService.delPost(id)
            } catch (e: Exception) {
                Log.d("dbbd", e.toString())
            }
        }
    }

}