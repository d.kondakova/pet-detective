package com.example.pd_android.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.example.pd_android.R
import com.example.pd_android.api.model.ApiDataModel
import com.example.pd_android.databinding.ListItemBinding

class FindingAdapter(
    private val itemClickCallback: (Int) -> Unit,
    private val itemClickCallback2: (ApiDataModel) -> Unit
) :
    ListAdapter<ApiDataModel, FindingAdapter.ViewHolder>(
        DiffCallback
    ) {

    inner class ViewHolder(
        private val view: ListItemBinding,
    ) : RecyclerView.ViewHolder(view.root) {
        fun bind(
            item: ApiDataModel,
            clickCallback: (Int) -> Unit,
            clickCallback2: (ApiDataModel) -> Unit
        ) {
            view.textView.text = "${item.kindOfAnimal} (${item.colorOfAnimal})"
            view.textView2.text = item.city
            view.button2.setOnClickListener { clickCallback(item.id) }
            view.root.setOnClickListener { clickCallback2(item) }
            /*view.imageView.load(item.filename) {
                placeholder(R.drawable.ic_pokeball).error(
                    R.drawable.ic_pokeball
                )
            }*/
            view.imageView.setImageResource(R.drawable.ic_pokeball)

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ListItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(currentList[position], itemClickCallback, itemClickCallback2)
    }

    object DiffCallback : DiffUtil.ItemCallback<ApiDataModel>() {
        override fun areItemsTheSame(oldItem: ApiDataModel, newItem: ApiDataModel): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: ApiDataModel,
            newItem: ApiDataModel
        ): Boolean {
            return oldItem == newItem
        }
    }

}