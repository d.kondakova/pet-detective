package com.example.pd_android.api.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import kotlinx.serialization.Serializable

@Serializable
@Parcelize
data class ApiDataModel(
    val id: Int,
    val kindOfAnimal: String,
    val colorOfAnimal: String,
    val city: String,
    val district: String,
    val author: String,
    val creationDate: String?,
    val type: Int,
    val filename: String?
) : Parcelable