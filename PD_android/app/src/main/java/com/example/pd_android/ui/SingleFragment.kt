package com.example.pd_android.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.pd_android.R
import com.example.pd_android.api.model.ApiDataModel
import com.example.pd_android.databinding.FragmentFindingBinding
import com.example.pd_android.databinding.FragmentSingleBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SingleFragment : Fragment() {

    private lateinit var binding: FragmentSingleBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSingleBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val data: ApiDataModel? = arguments?.getParcelable<ApiDataModel>("data")
        if (data != null) {
            binding.author.text = data.author
            binding.city.text = data.city
            binding.colorOfAnimal.text = data.colorOfAnimal
            binding.creationDate.text = data.creationDate
            binding.district.text = data.district
            binding.kindOfAnimal.text = data.kindOfAnimal
            binding.imageView2.setImageResource(R.drawable.ic_pokeball)
        }
    }

}