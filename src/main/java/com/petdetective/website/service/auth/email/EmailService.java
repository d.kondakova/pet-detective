package com.petdetective.website.service.auth.email;

import lombok.NonNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailService implements IEmailService {
    private final JavaMailSender emailSender;

    @Value("${spring.mail.username}")
    private final String senderName;

    public EmailService(@Value("${spring.mail.username}") final String senderName,
                        final JavaMailSender emailSender) {
        this.emailSender = emailSender;
        this.senderName = senderName;
    }

    @Override
    public void sendMessage(@NonNull final String[] to,
                            @NonNull final String subject,
                            @NonNull final String text) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(senderName);
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        emailSender.send(message);
    }

    @Override
    public void sendMessage(@NonNull final String to,
                            @NonNull final String subject,
                            @NonNull final String body) {
        sendMessage(new String[]{to}, subject, body);
    }
}
