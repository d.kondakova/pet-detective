package com.petdetective.website.service.auth.user;

import com.petdetective.website.models.entity.Role;
import com.petdetective.website.models.entity.User;
import com.petdetective.website.repo.RoleRepository;
import com.petdetective.website.repo.UserRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserService implements IUserService {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;

    private static final String UNCONFIRMED_USER_ROLE_NAME = "unconfirmed";
    private static final String REGISTERED_USER_ROLE_NAME = "registered";

    @Override
    public User findByEmail(@NonNull final String email) {
        return userRepository.findByEmail(email).orElse(null);
    }

    @Override
    public User findById(int id) {
        return userRepository.findById(id).orElse(null);
    }

    @Override
    public User register(User user) {
        final List<Role> roles = Collections.singletonList(
                roleRepository.findByName(UNCONFIRMED_USER_ROLE_NAME)
        );
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setRoles(roles);
        return userRepository.save(user);
    }
}

