package com.petdetective.website.service.auth.email;

public interface IEmailService {
    void sendMessage(String[] to, String subject, String body);
    void sendMessage(String to, String subject, String body);
}