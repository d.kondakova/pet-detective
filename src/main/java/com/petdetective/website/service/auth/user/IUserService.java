package com.petdetective.website.service.auth.user;

import com.petdetective.website.models.entity.User;

public interface IUserService {
    User findByEmail(String email);
    User findById(int id);

    User register(User user);
}
