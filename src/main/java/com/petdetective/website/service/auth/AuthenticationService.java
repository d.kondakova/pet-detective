package com.petdetective.website.service.auth;

import com.petdetective.website.models.dto.EmailRestoreDto;
import com.petdetective.website.models.dto.NewPasswordDto;
import com.petdetective.website.models.entity.PasswordRestoreToken;
import com.petdetective.website.properties.AddressProperties;
import com.petdetective.website.service.auth.errors.UserAlreadyRegisteredException;
import com.petdetective.website.service.auth.errors.UserNotFoundException;
import com.petdetective.website.service.auth.email.IEmailService;
import com.petdetective.website.service.auth.jwt.JwtTokenProvider;
import com.petdetective.website.models.Credentials;
import com.petdetective.website.models.dto.UserRegistrationDto;
import com.petdetective.website.models.entity.User;
import com.petdetective.website.service.auth.token.PasswordRestoreTokenService;
import com.petdetective.website.service.auth.user.IUserService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.petdetective.website.models.converters.UserConverter.convertToEntity;


@Service
@RequiredArgsConstructor
@Slf4j
public class AuthenticationService {
    @Lazy
    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    private final IUserService userService;
    private final IEmailService emailService;
    private final AddressProperties addressProperties;
    private final PasswordRestoreTokenService passwordRestoreTokenService;
    private final PasswordEncoder bCryptPasswordEncoder;

    @Transactional
    public String generateToken(@NonNull final Credentials credentials) {
        final String login = credentials.getLogin();
        final String password = credentials.getPassword();

        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(login, password));

        final User user = userService.findByEmail(login);
        log.info("User {} logged in", login);

        return jwtTokenProvider.createToken(user);
    }

    public void registerUser(@NonNull final UserRegistrationDto userRegistrationDto) {
        checkUniqueEmail(userRegistrationDto.getEmail());
        userService.register(convertToEntity(userRegistrationDto));
    }

    public void restore(@NonNull final EmailRestoreDto emailRestoreDto) {
        final User user = userService.findByEmail(emailRestoreDto.getEmail());
        if (null == user) {
            throw new UserNotFoundException();
        }
        final String address = addressProperties.getDomainName();
        final int port = addressProperties.getPort();

        final PasswordRestoreToken passwordRestoreToken = passwordRestoreTokenService
                .getPasswordRestoreToken(user.getId());

        new Thread(
                () -> {
                    try {
                        final String confirmUrl = String.format("http://%s:%d/auth/restore/%s",
                                address, port, passwordRestoreToken.getStringToken());
                        final String message = String.format("Hello, %s!\nPlease, click on following link " +
                                        "to change password:\n%s", user.getName(), confirmUrl);
                        emailService.sendMessage(emailRestoreDto.getEmail(), "Change password", message);
                    }
                    catch (final Exception e) {
                        log.error("Error sending password restore link to user: {}", user.getEmail(), e);
                    }
                }
        ).start();
    }

    @Transactional
    public void changePassword(@NonNull final String token,
                               @NonNull final NewPasswordDto newPasswordDto) {
        final int userId = passwordRestoreTokenService.getUserIdFromToken(token);
        final User user = userService.findById(userId);
        if (null == user) {
            throw new UserNotFoundException();
        }
        user.setPassword(bCryptPasswordEncoder.encode(newPasswordDto.getPassword()));
        passwordRestoreTokenService.delete(userId);
    }

    private void checkUniqueEmail(@NonNull final String email) {
        if (null != userService.findByEmail(email)) {
            throw new UserAlreadyRegisteredException();
        }
    }
}
