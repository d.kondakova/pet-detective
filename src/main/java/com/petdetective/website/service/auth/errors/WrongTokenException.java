package com.petdetective.website.service.auth.errors;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.FORBIDDEN)
public class WrongTokenException extends RuntimeException {
    public WrongTokenException() {
        super("Wrong token");
    }
}
