package com.petdetective.website.service.auth.jwt;

import com.petdetective.website.models.entity.User;
import com.petdetective.website.service.auth.user.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service("JwtUserDetailService")
@RequiredArgsConstructor
@Slf4j
public class JwtUserDetailService implements UserDetailsService {
    private final UserService userService;

    @Override
    public UserDetails loadUserByUsername(String login) {
        final User user = userService.findByEmail(login);
        if (user == null) {
            throw new UsernameNotFoundException("User with login " + login + " not found");
        }
        return JwtUserFactory.create(user);
    }
}

