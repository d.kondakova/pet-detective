package com.petdetective.website.service.auth.errors;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.FORBIDDEN, reason = "Token was expired")
public class TokenExpiredException extends RuntimeException {
    public TokenExpiredException() {
        super("Token was expired");
    }
}
