package com.petdetective.website.service.auth.errors;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.FORBIDDEN, reason="User already registered")
public class UserAlreadyRegisteredException extends RuntimeException {
}

