package com.petdetective.website.service.auth.token;

import com.petdetective.website.models.entity.PasswordRestoreToken;
import com.petdetective.website.repo.PasswordRestoreTokenRepository;
import com.petdetective.website.service.auth.errors.TokenExpiredException;
import com.petdetective.website.service.auth.errors.WrongTokenException;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

@Service
public class PasswordRestoreTokenService {
    @Value("${jwt.token.expired}")
    private final Long validityInMilliSeconds;
    private final PasswordRestoreTokenRepository passwordRestoreTokenRepository;

    @Autowired
    public PasswordRestoreTokenService(
            @Value("${jwt.token.expired}") Long validityInMilliSeconds,
            @NonNull final PasswordRestoreTokenRepository passwordRestoreTokenRepository) {
        this.validityInMilliSeconds = validityInMilliSeconds;
        this.passwordRestoreTokenRepository = passwordRestoreTokenRepository;
    }

    public PasswordRestoreToken getPasswordRestoreToken(final int userId) {
        final PasswordRestoreToken passwordRestoreToken = new PasswordRestoreToken();
        passwordRestoreToken.setUserId(userId);
        passwordRestoreToken.setExpirationDate(new Date(new Date().getTime() + validityInMilliSeconds));
        passwordRestoreToken.setStringToken(UUID.randomUUID().toString());
        return passwordRestoreTokenRepository.save(passwordRestoreToken);
    }

    public int getUserIdFromToken(@NonNull final String stringToken) {
        final PasswordRestoreToken passwordRestoreToken =
                passwordRestoreTokenRepository.findByStringToken(stringToken);
        validate(passwordRestoreToken);
        return passwordRestoreToken.getUserId();
    }

    public void delete(final int userId) {
        passwordRestoreTokenRepository.deleteByUserId(userId);
    }

    private void validate(final PasswordRestoreToken passwordRestoreToken) {
        if (passwordRestoreToken == null) {
            throw new WrongTokenException();
        }

        if (isExpired(passwordRestoreToken)) {
            throw new TokenExpiredException();
        }
    }

    private boolean isExpired(@NonNull final PasswordRestoreToken passwordRestoreToken) {
        return passwordRestoreToken.getExpirationDate().before(new Date());
    }
}