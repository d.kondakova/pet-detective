package com.petdetective.website.controllers.auth;

import com.petdetective.website.models.Credentials;
import com.petdetective.website.models.dto.EmailRestoreDto;
import com.petdetective.website.models.dto.NewPasswordDto;
import com.petdetective.website.models.dto.TokenDto;
import com.petdetective.website.models.dto.UserRegistrationDto;
import com.petdetective.website.service.auth.AuthenticationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/auth")
@RequiredArgsConstructor
@Slf4j
public class AuthenticationController {
    private final AuthenticationService authenticationService;

    @PostMapping("/sign-in")
    public TokenDto signIn(@RequestBody final Credentials credentials) {
        return new TokenDto(
                authenticationService.generateToken(credentials)
        );
    }

    @PostMapping("/sign-up")
    public void signUp(@RequestBody final UserRegistrationDto userRegistrationDto) {
        authenticationService.registerUser(userRegistrationDto);
    }

    @PostMapping("restore")
    public void restorePassword(@RequestBody final EmailRestoreDto emailRestoreDto) {
        authenticationService.restore(emailRestoreDto);
    }

    @PostMapping("/restore/{token}")
    public void restore(@RequestBody final NewPasswordDto newPasswordDto,
                        @PathVariable final String token) {
        authenticationService.changePassword(token, newPasswordDto);
    }
}
