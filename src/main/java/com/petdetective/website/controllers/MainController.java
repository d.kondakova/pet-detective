package com.petdetective.website.controllers;

import com.petdetective.website.models.Advert;
import com.petdetective.website.repo.AdvertRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.security.Principal;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.UUID;

@RestController
@RequestMapping("")
public class MainController {

    @Autowired
    private final AdvertRepository advertRepository;

    public MainController(AdvertRepository advertRepository) {
        this.advertRepository = advertRepository;
    }

    @Value("${upload.path}")
    private String uploadPath;

    @GetMapping("missing")
    public Iterable<Advert> missing_list(final Principal principal) {
        /* TODO: Идентифицируем пользователя по почте. Principal содержит почту. По ней ищем User'а */
        System.out.println("Username: " + principal.getName());

        return advertRepository.findByTypeOrderByCreationDateDesc(1);
    }

    @GetMapping("finding")
    public Iterable<Advert> finding_list() {
        return advertRepository.findByTypeOrderByCreationDateDesc(2);
    }

    @GetMapping("{id}")
    public Advert missing(@PathVariable("id") Advert advert) {
        return advert;
    }

    @PostMapping("add_missing")
    public int create_missing(@RequestBody Advert advert/*,
                                 @RequestParam("file")MultipartFile file*/) throws IOException {
        advert.setCreationDate(LocalDateTime.now());
        advert.setType(1);

        /*if (file != null) {
            File uploadDir = new File(uploadPath);

            if (!uploadDir.exists()) {
                uploadDir.mkdir();
            }
            String uuidFile = UUID.randomUUID().toString();
            String resultFilename = uuidFile + "." + file.getOriginalFilename();

            file.transferTo(new File(uploadPath + "/" + resultFilename));

            advert.setFilename(resultFilename);
        } else {
            advert.setFilename("slon.jpeg");
        }*/

        advertRepository.save(advert);

        return 1;
    }

    @PostMapping("add_finding")
    public int create_finding(@RequestBody Advert advert/*,
                                 @RequestParam("file")MultipartFile file*/) throws IOException {
        advert.setCreationDate(LocalDateTime.now());
        advert.setType(2);

        /*if (file != null) {
            File uploadDir = new File(uploadPath);

            if (!uploadDir.exists()) {
                uploadDir.mkdir();
            }
            String uuidFile = UUID.randomUUID().toString();
            String resultFilename = uuidFile + "." + file.getOriginalFilename();

            file.transferTo(new File(uploadPath + "/" + resultFilename));

            advert.setFilename(resultFilename);
        } else {
            advert.setFilename("slon.jpeg");
        }*/

        advertRepository.save(advert);

        return 1;
    }

    @GetMapping("missing_filter")
    public Iterable<Advert> missing_filter(@RequestParam String kindOfAnimal, @RequestParam String colorOfAnimal,
                                           @RequestParam String city, @RequestParam String district) {

        return advertRepository.findByKindOfAnimalContainingAndColorOfAnimalContainingAndCityContainingAndDistrictContainingAndTypeOrderByCreationDateDesc
                (kindOfAnimal, colorOfAnimal, city, district, 1);
    }

    @GetMapping("finding_filter")
    public Iterable<Advert> finding_filter(@RequestParam String kindOfAnimal, @RequestParam String colorOfAnimal,
                                           @RequestParam String city, @RequestParam String district) {

        return advertRepository.findByKindOfAnimalContainingAndColorOfAnimalContainingAndCityContainingAndDistrictContainingAndTypeOrderByCreationDateDesc
                (kindOfAnimal, colorOfAnimal, city, district, 2);
    }

    @PutMapping("{id}")
    public Advert update(@PathVariable("id") Advert advertFromDB,
                         @RequestBody Advert advert) {
        BeanUtils.copyProperties(advert, advertFromDB, "id");

        return advertRepository.save(advertFromDB);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable("id") Advert advert) {
        advertRepository.delete(advert);
    }
}
