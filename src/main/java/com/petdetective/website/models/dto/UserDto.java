package com.petdetective.website.models.dto;

import lombok.*;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class UserDto {
    private String name;
    private String email;
}
