package com.petdetective.website.models.dto;

import lombok.Data;

@Data
public class NewPasswordDto {
    private String password;
}
