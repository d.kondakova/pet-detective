package com.petdetective.website.models.dto;

import lombok.Data;

@Data
public class EmailRestoreDto {
    private String email;
}
