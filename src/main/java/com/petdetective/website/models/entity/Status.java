package com.petdetective.website.models.entity;

public enum Status {
    ACTIVE, NOT_ACTIVE, DELETED
}
