package com.petdetective.website.models.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "password_restore_tokens")
@Setter
@Getter
public class PasswordRestoreToken {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String stringToken;

    private int userId;

    @Temporal(TemporalType.TIMESTAMP)
    private Date expirationDate;
}