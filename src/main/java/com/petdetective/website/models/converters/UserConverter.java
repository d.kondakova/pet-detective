package com.petdetective.website.models.converters;

import com.petdetective.website.models.dto.UserDto;
import com.petdetective.website.models.dto.UserRegistrationDto;
import com.petdetective.website.models.entity.User;
import lombok.NonNull;

public class UserConverter {
    public static User convertToEntity(@NonNull final UserRegistrationDto registrationDto) {
        final User user = new User();
        user.setName(registrationDto.getName());
        user.setEmail(registrationDto.getEmail());
        user.setPassword(registrationDto.getPassword());
        return user;
    }

    public static UserDto convertToDto(@NonNull final User user) {
        return new UserDto(user.getName(), user.getEmail());
    }
}
