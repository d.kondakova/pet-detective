package com.petdetective.website.models;

import lombok.Data;

@Data
public class Credentials {
    private String login;
    private String password;
}
