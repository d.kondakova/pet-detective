package com.petdetective.website.properties;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Data
public class AddressProperties {
    private final int port;
    private final String domainName;

    public AddressProperties(@Value("${server.port}") final int port,
                             @Value("${server.domainName}") final String domainName) {
        this.port = port;
        this.domainName = domainName;
    }
}