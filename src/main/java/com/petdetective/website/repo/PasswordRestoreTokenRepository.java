package com.petdetective.website.repo;

import com.petdetective.website.models.entity.PasswordRestoreToken;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PasswordRestoreTokenRepository
        extends CrudRepository<PasswordRestoreToken, Integer> {
    PasswordRestoreToken findByStringToken(String token);

    void deleteByUserId(int userId);
}