package com.petdetective.website.repo;

import com.petdetective.website.models.Advert;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AdvertRepository extends JpaRepository<Advert, Long> {
    List<Advert> findByTypeOrderByCreationDateDesc(int type);

    List<Advert> findByKindOfAnimalContainingAndColorOfAnimalContainingAndCityContainingAndDistrictContainingAndTypeOrderByCreationDateDesc
            (String kindOfAnimal, String colorOfAnimal, String city, String district, int type);
}
